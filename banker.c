
// banker.c
//
// Tyler Manifold & Isaac Murillo
// CSCI 403
// HMW4
//
// "Banker's Algorithm
//  For this project you will write a multithreaded program that implements the banker's
//  algorithm discussed in section 7.5.3. Several customers request and release resources
//  from the bank. The banker will grant a request only if it leaes the system in a safe
//  state. A request that leaves the system in an unsade state will be denied."
//  -- Operating Systems Concepts, p345


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

typedef enum {false, true} bool;

// The Banker
//
// The banker will consider requests from n customers for m resources types as outlined
// in section 7.5.3. The banker will keep track of resources using the following data
// structures

// these may be any values >= 0
#define NUMBER_OF_CUSTOMERS 5
#define NUMBER_OF_RESOURCES 3

// the vailable amount of each resource
int available[NUMBER_OF_RESOURCES];

// the maximum demand of each customer
int maximum[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];

// the amount currently allocated to each customer
int allocation[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];

// the remaining need of each customer
int need[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];


// The Customer
//
// Create n customer threads that request and release resources from the bank.
// The customers will continually loop, requesting then releasing random numbers of
// resources. The customers' requests for resources will be bounded by their respective
// values in the `need` array. The banker will grant a request if it satisfies the safety
// algorithm outlined in section 7.5.3.1. If a request does not leave the system in a safe
// state, the banker will deny it.

pthread_t customers[NUMBER_OF_CUSTOMERS];
int thread_numbers[NUMBER_OF_CUSTOMERS];
void customer_init(void*);

// These functions should return 0 if successful or -1 if unsuccessful.
int request_resources(int customer_num, int request[]);
int release_resources(int customer_num, int release[]);

// Multiple threads (customers) will access shared data through these functions.
// Therefore, access must be controlled through mutex locks to prevent race conditions.
pthread_mutex_t lock;

// prototypes for other utility functions can go here
void alloc_res(int, int, int);
void free_res(int, int, int);

int isSafe();

void show_available()
{
	printf("available: ");
	for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
	{
		printf("%d ", available[i]);
	}
	printf("\n");
}

void show_maximum()
{
	printf("maximum:\n\t");
	for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
	{
		for (int j = 0; j < NUMBER_OF_RESOURCES; j++)
		{
			printf("%2d ", maximum[i][j]);
		}
		printf("\n\t");
	}
	printf("\n");
}

void show_need()
{
	printf("need:\n\t");
	for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
	{
		for (int j = 0; j < NUMBER_OF_RESOURCES; j++)
		{
			printf("%2d ", need[i][j]);
		}
		printf("\n\t");
	}
	printf("\n");
}


int main(int argc, char** argv)
{
	if (argc != NUMBER_OF_RESOURCES + 1)
	{
		printf ("invalid number of arguments.\n");
	}
	else
	{

		pthread_mutex_init(&lock, NULL);

		srand(time(NULL));

		// initialize available array to number of resources specified
		for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
		{
			available[i] = atoi(argv[i+1]);
		}

		show_available();

		// initialize maximum array with random numbers less than available array
		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			for (int j = 0; j < NUMBER_OF_RESOURCES; j++)
			{
				maximum[i][j] = rand() % available[j] + 1;
			//	printf("maximum[%d][%d] = %d\n", i, j, maximum[i][j]);
			}
		}

		show_maximum();

		// initialize need
		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			for (int j = 0; j < NUMBER_OF_RESOURCES; j++)
			{
				need[i][j] = maximum[i][j] - allocation[i][j];
			}
		}

		show_need();

		// spawn customer threads
		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			thread_numbers[i] = i;
			pthread_create(&customers[i], NULL, (void*) customer_init, (void*) &thread_numbers[i]);
		}

		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			pthread_join(customers[i], NULL);
		}

		printf("\nAll jobs completed successfully.\n");
	}

	return 0;
}

void customer_init(void* n)
{
	int *customer_num = (int*) n;
	
	int request[NUMBER_OF_RESOURCES];

	bool has_resources;

	int max_wait = 5;

	int max_attempts = 5;
	int attempts = 0;

	while (true)
	{	
		// begin assuming we have all of our resources
		has_resources = true;

		// randomly generate a request array constrained by need such that:
		//     request[i] <= need[customer_num][i]

		for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
		{
			if (need[*customer_num][i] > 0)
			{
				do
				{
					request[i] = rand() % need[*customer_num][i] + 1;

				} while (request[i] > need[*customer_num][i]);
			}
			else
			{
				request[i] = 0;
			}
		}

		int req = request_resources(*customer_num, request);

		int s = 0;

		if (req < 0)
		{
			attempts++;
			s = rand() % max_wait + 1;

			printf("%s %d: Request denied. %d/%d attempts. Waiting %d second(s).\n\n", __func__, *customer_num, attempts, max_attempts, s);

		
			// request was denied. thread will sleep between 1 and max_wait seconds before making a new request.
			sleep(s);
		}
		else
		{
			printf("%s %d: Request approved.\n\n", __func__, *customer_num);
			attempts = 0;
		}

		// check if we have all of our resources.
		// if we need > 0 then we don't have all of the resources
		for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
		{
			if (need[*customer_num][i] > 0)
				has_resources = false;
		}

		// if we have all of our resources, simulate the process completing by
		//   releasing all resources allocated to this proc
		// if we have made the maximum number of attempts to get resources, relinquish all of our resources
		if (has_resources || attempts == max_attempts)
		{
			printf("%s: Proc %d relinquishing resources. Reason: %s\n\n", __func__, *customer_num, (attempts == max_attempts) ? "Max attempts" : "Job finished");
			
			int release[NUMBER_OF_RESOURCES];

			for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
				release[i] = allocation[*customer_num][i];

			release_resources(*customer_num, release);
			attempts = 0;

			if (has_resources)
				pthread_exit(NULL);
		}
	}
}

// allocate the resources to customer_num
void alloc_res(int customer_num, int resource, int amount)
{
	available[resource] = available[resource] - amount;
	allocation[customer_num][resource] = allocation[customer_num][resource] + amount;
	need[customer_num][resource] = need[customer_num][resource] - amount;

	printf ("%s: P%d <- %d R%d. ", __func__, customer_num, amount, resource);
	show_available();
	//printf ("%s: Allocated %d of resource %d to process %d. Available: %d\n", __func__, amount, resource, customer_num, available[resource]);
}

// free the specified resource from customer_num
void free_res (int customer_num, int resource, int amount)
{
	available[resource] = available[resource] + amount;
	allocation[customer_num][resource] = allocation[customer_num][resource] - amount;
	need[customer_num][resource] = need[customer_num][resource] + amount;

	printf ("%s: P%d -> %d R%d. ", __func__, customer_num, amount, resource);
	show_available();

	//printf ("%s: Freed %d of resource %d from process %d. Available: %d\n", __func__, amount, resource, customer_num, available[resource]);
}

// resource request algorithm (p.332):
// 	1. If Request_i <= Need_i go to step 2. Otherwise, raise an error condition, since the process has
// 	exceeded its maximum claim.
// 	2. If Request_i <= Available, go to step 3. Otherwise, P_i must wait, since the resources are not
// 	available.
// 	3. Have the system pretend to have allocated the requested resources to process P_i by modifying the
// 	state as follows:
// 		Available = Available - Request;
// 		Allocation_i = Allocation_I + Request;
// 		Need_i = Need_i - Request_i;
int request_resources(int customer_num, int request[])
{	
	int r = 0;
	
	pthread_mutex_lock(&lock);

	while (r < NUMBER_OF_RESOURCES)
	{

		// only print request message for non-zero requests
		if (request[r] > 0)
		{
			printf("%s: Proc: %d, Request: %d, Resource: %d. Need: %d, Max: %d, Available: %d\n", __func__,
				customer_num, request[r], r, need[customer_num][r], maximum[customer_num][r], available[r]);
		}

		if (request[r] > maximum[customer_num][r])
		{
			printf("%s: Process %d exceeded its maximum claim.\n", __func__, customer_num);
			pthread_mutex_unlock(&lock);

			return -1;
		}
		else if (request[r] > need[customer_num][r])
		{
			printf("%s: Process %d requested more than it needs. %d > %d\n", __func__, customer_num, request[r], need[customer_num][r]);
			pthread_mutex_unlock(&lock);

			return -1;
		}
		else if (request[r] > 0)
		{
			if (request[r] > available[r])
			{
				// must wait for resources to become available
				printf("%s: Not enough resources available.\n", __func__);
				pthread_mutex_unlock(&lock);

				return -1;
			}
			else
			{
				// pretent to have allocated the resources
				alloc_res(customer_num, r, request[r]);

				if (isSafe() == false)
				{
					printf ("%s: Transaction is unsafe.\n", __func__, customer_num);
					
					free_res(customer_num, r, request[r]);

					pthread_mutex_unlock(&lock);
					
					return -1;
				}
				else
				{
					printf("%s: Transaction is safe.\n", __func__, customer_num);
				}
			}
		}
		r++;
	}

	pthread_mutex_unlock(&lock);
	return 0;
}

// free the specifed number of resources
int release_resources(int customer_num, int release[])
{
	pthread_mutex_lock(&lock);

	for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
	{
		available[i] = available[i] + release[i];
		allocation[customer_num][i] = allocation[customer_num][i] - release[i];
		need[customer_num][i] = need[customer_num][i] + release[i];
	}

	printf ("%s: Proc %d released resources: ", __func__, customer_num);

	for (int i = 0; i < NUMBER_OF_RESOURCES; i++)
	{
		printf("%d ", release[i]);
	}

	printf(". ");
	show_available();

	pthread_mutex_unlock(&lock);

	return 1;
}

// Safety Algorithm
// - find out if the system state is safe, or not safe
// 1. Inititalize work = available and Finish[i] = false
// 	- Work keeps a running tally of how resources are allocated
// 	- Finish indicates if Process P_i has enough resources to complete
// 2. Find an indiex i such that:
// 	a. Finish[i] == false
// 	b. Need_i <= Work
// 	If no such i exists, go to step 4.
// 3. Work = Work + Allocation
//    Finish[i] = true
//    go to step 2.
// 4. If Finish[i] == true for all i, then the system is in a safe state
int isSafe(){

	int work[NUMBER_OF_RESOURCES];
	int finish[NUMBER_OF_CUSTOMERS];


	//-------------------------------------------------------------

	/*
	*2. While all process are not in a safe state------------------
	*/
	int counter = 0;
	int found;

	
	for(int x = 0; x < NUMBER_OF_CUSTOMERS; x++)
	{

		//Setting finish array to false.
		for(int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			finish[i] = 0;
		}
		
		//Creating a copy of available resources 
		for(int i = 0; i < NUMBER_OF_RESOURCES; i++)
		{
			work[i] = available[i];
		}
		found = true;

		for(int y = 0; y < NUMBER_OF_CUSTOMERS; y++)
		{
			//2. a) Finish[i] ==  false
			if(finish[y] == 0)
			{
				//printf("%s: finish[%d] = 0.\n", __func__, y);
				/*2. b)
				 *Checking for all resources for current customers 
				 *need <= work
				*/
				int RC = 0; 

				for(int j = 0; j < NUMBER_OF_RESOURCES; j++)
				{
					if(need[y][j] <= work[y])
					{
						//printf ("%s: need[%d][%d] < work[%d]. Incrementing RC.\n", __func__, y, j, y);
						RC++;
					}
				}

				//printf ("%s %d: rc = %d.\n", __func__, x, RC);
				//If all needs were satisfied
				if(RC  == NUMBER_OF_RESOURCES)
				{
					//printf ("%s: RC = %d = NUMBER_OF_RESOURCES.\n", __func__, RC);
					//Mark customer as done and move onto next
					counter++;
					finish[y] = 1;

					/*add allocated rescouorces of current customer
					 *to available work resources
					*/
					for(int k = 0; k < NUMBER_OF_RESOURCES; k++)
					{
						work[k] += allocation[y][k];
					}
				}

			}
			if (counter == NUMBER_OF_CUSTOMERS)
			{
				return 1;
			}
		}

/*
		printf("%s %d: " , __func__,x);
		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			printf("%d ", finish[i]);
		}
		printf("\n");

		for (int i = 0; i < NUMBER_OF_CUSTOMERS; i++)
		{
			if (finish[i] == 0)
			{
				found = false;
				break;
			}

		}
		*/
	}

//	return found;
//	return 0;

}
