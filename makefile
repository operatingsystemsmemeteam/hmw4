
# makefile for banker
#
# Tyler Manifold & Isaac Murillo

CXX = gcc
STD = gnu99

banker: banker.c
	$(CXX) -lpthread $^ -std=$(STD) -o $@

clean:
	rm banker
	rm banker.log

run:
	./banker 10 5 7

log:
	./banker 10 5 7 >> banker.log
